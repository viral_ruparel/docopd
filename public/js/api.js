// The Api module is designed to handle all interactions with the server
/* global XMLHttpRequest*/

function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}


var Api = (function () {
  var uuid = guid();
  var requestPayload;
  var responsePayload;

  var messageEndpoint = '/api/message';
  var hindiMessageEndpoint = '/api/hindiMessage';

  //Send a message request to the server
  function sendRequest(text, context) {
    // Build request payload
    var payloadToWatson = {};
    if (text) {
      payloadToWatson.input = {
        text: text
      };
    }
    if (context) {
      payloadToWatson.context = context;
    }
    payloadToWatson.user = uuid;

    // Built http request
    var http = new XMLHttpRequest();
    http.open('POST', messageEndpoint, true);
    http.setRequestHeader('Content-type', 'application/json');
    http.onreadystatechange = function () {
      if (http.readyState === 4 && http.status === 200 && http.responseText) {
        Api.setResponsePayload(http.responseText);
      }
    };

    var params = JSON.stringify(payloadToWatson);
    console.log("***********************" + params + "********************************");

    // Stored in variable (publicly visible through Api.getRequestPayload)
    // to be used throughout the application
    if (Object.getOwnPropertyNames(payloadToWatson).length !== 0) {
      Api.setRequestPayload(params);
    }

    // Send request
    http.send(params);
  }


  //Send a message request to the server
  function sendHindiRequest(text, context) {
    // Build request payload
    var payloadToWatson = {};
    if (text) {
      payloadToWatson.input = {
        text: text
      };
    }
    if (context) {
      payloadToWatson.context = context;
    }
    payloadToWatson.user = uuid;

    // Built http request
    var http = new XMLHttpRequest();
    http.open('POST', hindiMessageEndpoint, true);
    http.setRequestHeader('Content-type', 'application/json');
    http.onreadystatechange = function () {
      if (http.readyState === 4 && http.status === 200) {
        console.log("Response from Dialogflow : ", http.response);
        Api.setResponsePayload(http.response);
      }
    };

    var params = JSON.stringify(payloadToWatson);
    console.log("***********************" + params + "********************************");

    // Stored in variable (publicly visible through Api.getRequestPayload)
    // to be used throughout the application
    if (Object.getOwnPropertyNames(payloadToWatson).length !== 0) {
      Api.setRequestPayload(params);
    }

    // Send request
    http.send(params);
  }


  function sendFeedback(textrating, textcomment) {

    var messagefeedbackendpoint = '/api/feedback';
    var httpfeedback = new XMLHttpRequest();
    var parametertoCloudant = {};
    parametertoCloudant.input = {
      textrating: textrating,
      textcomment: textcomment
    };
    var paramsfeedback = JSON.stringify(parametertoCloudant);
    httpfeedback.open('POST', messagefeedbackendpoint, true);
    httpfeedback.setRequestHeader('Content-type', 'application/json');
    httpfeedback.onreadystatechange = function () {
      if (httpfeedback.readyState === 4 && httpfeedback.status === 200 && httpfeedback.responseText) {
      }
    };
    httpfeedback.send(paramsfeedback);
  }


  // Publicly accessible methods defined
  return {
    sendRequest: sendRequest,
    sendHindiRequest: sendHindiRequest,
    sendFeedback: sendFeedback,

    // The request/response getters/setters are defined here to prevent internal methods
    // from calling the methods without any of the callbacks that are added elsewhere.
    getRequestPayload: function () {
      return requestPayload;
    },
    setRequestPayload: function (newPayloadStr) {
      requestPayload = JSON.parse(newPayloadStr);
    },
    getResponsePayload: function () {
      return responsePayload;
    },
    setResponsePayload: function (newPayloadStr) {
      console.log("*******************Set payload: ", newPayloadStr, "DONE********************");
      responsePayload = JSON.parse(newPayloadStr);
    }
  };

}());
