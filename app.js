'use strict';
require('dotenv').config({
  silent: true
});
var express = require('express'); // app server
var bodyParser = require('body-parser'); // parser for post requests
var Conversation = require('ibm-watson/assistant/v1'); // watson sdk
var Discovery = require('ibm-watson/discovery/v1');
const vcapServices = require('vcap_services');
const dateTime = require('date-time');
var CloudantLoad = require('./lib/CloudantLoad');
var Autolinker = require('autolinker');
var htmlToText = require('html-to-text');
var path = require('path');
var appDir = path.dirname(require.main.filename);
var findRemoveSync = require('find-remove');
var findInFiles = require('find-in-files');
const dialogflow = require('dialogflow');
const uuid = require('uuid');
//"watson-developer-cloud": "^4.0.1"
//************* CODE FOR OBJECT STORAGE *************
const util = require('util');
 //var skipper = require("skipper"),
 //"skipper": "^0.8.7",
 //"s3": "^4.4.0",
//"knox": "^0.9.2",
var skipperS3 = require('skipper-s3'),
  S3Lister = require("s3-lister");
var s3config = null
try {
  s3config = require("./s3-credentials.json");
}
catch (e) {} 

//************* CODE FOR OBJECT STORAGE *************

var response = null;
const fs = require('fs'); // file system for loading JSON
//
//
const sendmail = require('sendmail')({
  silent: true,
  dkim: {
    privateKey: fs.readFileSync('dkim-private.pem', 'utf8'),
    keySelector: 'mydomainkey'
  }
})
//
//var watson = require('watson-developer-cloud');

const WatsonDiscoverySetup = require('./lib/watson-discovery-setup');

var app = express();

// Bootstrap application settings
app.use(express.static('./public')); // load UI from public folder
app.use(bodyParser.json());

const DEFAULT_NAME = 'watson-jarvis-chatbot';
const DISCOVERY_ACTION = 'rnr'; // Replaced RnR w/ Discovery but Conversation action is still 'rnr'.
const DISCOVERY_DOCS = [];

const discoveryCredentials = vcapServices.getCredentials('discovery');

const discovery = new Discovery({
  password: discoveryCredentials.password,
  username: discoveryCredentials.username,
  //version_date: ,
  version: '2017-04-27'
});
let discoveryParams; // discoveryParams will be set after Discovery is validated and setup.
 
//Prints all this in console while starting the server Start.{
const discoverySetup = new WatsonDiscoverySetup(discovery);
const discoverySetupParams = {
  default_name: DEFAULT_NAME,
  documents: DISCOVERY_DOCS
};
discoverySetup.setupDiscovery(discoverySetupParams, (err, data) => {
  if (err) {
    handleSetupError(err);
  } else {
    console.log('Discovery is ready!');
    discoveryParams = data;
  }
});
//}Prints all this in console while starting the server End.

/**
 * Send a query to the dialogflow agent, and return the query result.
 * @param {string} projectId The project to be used
 */


//const projectId = 'project-id';
const clientOptions = {
  keyFilename: './DocOPD-fd128b871a83.json'
};

// Instantiates a session client
const sessionClient = new dialogflow.SessionsClient(clientOptions);
 
  
   
var autolinker = new Autolinker({
  urls: {
    schemeMatches: true,
    wwwMatches: true,
    tldMatches: true
  },
  email: true,
  phone: true,
  mention: false,
  hashtag: false,

  stripPrefix: true,
  stripTrailingSlash: true,
  newWindow: true,

  truncate: {
    length: 0,
    location: 'end'
  },

  className: ''
});

// Create the service wrapper
var conversation = new Conversation({
  // If unspecified here, the CONVERSATION_USERNAME and CONVERSATION_PASSWORD env properties will be checked
  // After that, the SDK will fall back to the bluemix-provided VCAP_SERVICES environment property
  // username: 'b46292c2-b8df-408d-825f-1b2114ac9c8c',
  // password: 'LGimsAmj2SrE',
  iam_apikey: process.env.ASSISTANT_APIKEY,
  url: process.env.ASSISTANT_URL,
  version: '2018-09-20'
  //disable_ssl_verification: true
});

var conversationidmaster = "";
let fullnamemaster = "";

app.get('/autocomplete/:search', function (req, res) {
  console.log(fullnamemaster);
  console.log();
  if (fullnamemaster != "") {
    var b = req.params.search;
    //console.log ('Querying:' +b +" CID"+fullnamemaster);
    findInFiles.find({
        'term': b,
        'flags': 'ig'
      }, './data/', '.txt$')
      .then(function (results) {
        //console.log (results.count );
        for (var result in results) {
          var resp = results[result];

          res.jsonp(resp.line.slice(0, 10));
          // console.log('found "' + resp.matches[0] + '" ' + resp.count+ ' times in "' + resp.line[1]+ '"' );
        }


      });
  }
});

// Record Feedback
app.post('/api/feedback', function (req, res) {
  var ratinglocal = "";
  var commnetlocal = ""
  if (req.body.input.textrating != undefined) {
    ratinglocal = req.body.input.textrating;

  }
  if (req.body.input.textcomment != undefined) {
    commnetlocal = req.body.input.textcomment;

  }
  //console.log (req);
  //console.log(res.body);
  if (ratinglocal != "" && commnetlocal != "" && conversationidmaster != "" && fullnamemaster != "") {

    sendfeedbackToCloudant(ratinglocal, commnetlocal, conversationidmaster, fullnamemaster, dateTime());
  }
  
  return res.json("Thank you for proving feedback.");
});


app.post('/api/report', function (req, res) {
  var pathlocal = '/report/';
  findRemoveSync(appDir + pathlocal, {
    age: {
      seconds: 172800
    },
    extensions: '.xlsx'
  });
  senddataToCloudantReport(req, res, (filename) => {
    return res.json(filename);
  })
  
});


app.post('/api/feedbackreport', function (req, res) {
  var pathlocal = '/report/';
  findRemoveSync(appDir + pathlocal, {
    age: {
      seconds: 172800
    },
    extensions: '.xlsx'
  });
  senddataToCloudantReportFeedback(req, res, (filename) => {
    return res.json(filename);
  })
  
});

app.get('/api/download', function (req, res) {
  var reportfilenamelocal = "";
  var arr = req.url.split("=");
  if (req.url != undefined) {

    var arr = req.url.split("=");
    reportfilenamelocal = arr[1];

  }
  if (reportfilenamelocal != "") {
    var pathlocal = '/report/';
    var reportfilenamelocalfullnamewithpath = appDir + pathlocal + reportfilenamelocal;
    if (checkfileExists(fs, reportfilenamelocalfullnamewithpath)) {
      var file = fs.createReadStream(reportfilenamelocalfullnamewithpath);
      var stat = fs.statSync(reportfilenamelocalfullnamewithpath);
      res.setHeader('Content-Length', stat.size);
      res.setHeader('Content-Type', 'application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
      res.setHeader('Content-Disposition', 'attachment; filename=' + reportfilenamelocal);
      file.pipe(res);

    } else {
      console.log('Download file not present');
    }
  }
});

app.post('/api/hindiMessage', function (req, res) {
  if (req.body.input) {
    req.body.input.text = htmlToText.fromString(req.body.input.text, {
      wordwrap: 130
    });
  }

  function detectTextIntent(projectId, sessionId, queries, languageCode) {
    // [START dialogflow_detect_intent_text]
  
    /**
     * TODO(developer): UPDATE these variables before running the sample.
     */
    // projectId: ID of the GCP project where Dialogflow agent is deployed
    // const projectId = 'PROJECT_ID';
    // sessionId: Random number or hashed user identifier
    // const sessionId = 123456;
    // queries: A set of sequential queries to be send to Dialogflow agent for Intent Detection
    // const queries = [
    //   'Reserve a meeting room in Toronto office, there will be 5 of us',
    //   'Next monday at 3pm for 1 hour, please', // Tell the bot when the meeting is taking place
    //   'B'  // Rooms are defined on the Dialogflow agent, default options are A, B, or C
    // ]
    // languaceCode: Indicates the language Dialogflow agent should use to detect intents
    // const languageCode = 'en';
  
    // Imports the Dialogflow library
    //const dialogflow = require('dialogflow');
  
    
  
    async function detectIntent(
      projectId,
      sessionId,
      query,
      contexts,
      languageCode
    ) {
     // The path to identify the agent that owns the created intent.
      const sessionPath = sessionClient.sessionPath(projectId, sessionId);

  
      // The text query request.
      const request = {
        session: sessionPath,
        queryInput: {
          text: {
            text: query,
            languageCode: languageCode,
          },
        },
      };
  
      if (contexts && contexts.length > 0) {
        request.queryParams = {
          contexts: contexts,
        };
      }
  
      const responses = await sessionClient.detectIntent(request);
      return responses[0];
    }
  
    async function executeQueries(projectId, sessionId, queries, languageCode) {
      // Keeping the context across queries let's us simulate an ongoing conversation with the bot
      let context;
      let intentResponse;
      for (const query of queries) {
        try {
          console.log(`Sending Query: ${query}`);
          intentResponse = await detectIntent(
            projectId,
            sessionId,
            query,
            context,
            languageCode
          );
          console.log('Detected intent');
          console.log(
            `Fulfillment Text: ${intentResponse.queryResult.fulfillmentText}`
          );
          console.log(intentResponse);
          var responseText = {"output": {"text": intentResponse.queryResult.fulfillmentText}};
          console.log(responseText);
          // Use the context from this response for next queries
          context = intentResponse.queryResult.outputContexts;
          console.log('Message>>>>> ', intentResponse.queryResult.fulfillmentMessages[0]);
          console.log('Context*********', context);
          //console.log('Parameters*********', context[0].parameters);
          return res.json(updateHindiMessage(responseText));
        } catch (error) {
          console.log("Error: ", error);
        }
      }
    }
    executeQueries(projectId, sessionId, queries, languageCode);
    // [END dialogflow_detect_intent_text]
  }

  detectTextIntent('docopd', req.body.user, [req.body.input.text] , 'hi');
});

// Endpoint to be call from the client side
app.post('/api/message', function (req, res) {
  fullnamemaster = '';
  var workspace = process.env.WORKSPACE_ID || '<workspace-id>';
  
  if (!workspace || workspace === '<workspace-id>') {
    return res.json({
      'output': {
        'text': 'The app has not been configured with a <b>WORKSPACE_ID</b> environment variable. Please refer to the ' + '<a href="https://github.com/watson-developer-cloud/conversation-simple">README</a> documentation on how to set this variable. <br>' + 'Once a workspace has been defined the intents may be imported from ' + '<a href="https://github.com/watson-developer-cloud/conversation-simple/blob/master/training/car_workspace.json">here</a> in order to get a working application.'
      }
    });
  }
  if (req.body.input) {
    req.body.input.text = htmlToText.fromString(req.body.input.text, {
      wordwrap: 130
    });
  }
  
  
  var payload = {
    workspace_id: workspace,
    context: req.body.context || {},
    input: req.body.input || {}

  };

  
  function processResponse(err, response) {
    if (err) {
      console.error(err); // something went wrong
      return;
    }

    var endConversation = false;
    
    

    if (response.context.action === process.env.CONNECT_CSR) {
      //console.log('Origin:' + response.context.origin);
      var to_email;
      if (response.context.origin == 'EUC') {
        to_email = process.env.EUC_EMAIL;
      } else if (response.context.origin == 'P2Y') {
        to_email = process.env.P2Y_EMAIL;

      } else if (response.context.origin == 'OTC') {
        to_email = process.env.OTC_EMAIL;

      }

      senddataconversationToCloudantReport(response.context.conversation_id, response.context.name, (filename) => {
        //console.log("ID:"+ response.context.conversation_id);
        var fpath = '/report/';
        console.log("email:" + to_email);
        // Send email
        if (to_email) {
          sendmail({
            from: process.env.FROM_EMAIL,
            to: to_email,
            subject: 'Jarvis Chat Transcript for ' + response.context.name,
            html: 'Dear Agent, <br>The following user:' + response.context.name + ' needs your assistance.<br> Please check the attached conversation log for details.<br><br>Always eager to help.<br><br>Yours,<br> Jarvis<br><br>',
            attachments: [{ // binary buffer as an attachment
              filename: filename,
              path: appDir + fpath + filename
            }]
          }, function (err, reply) {
            console.log(err && err.stack);
            console.dir(reply);
          });
          console.log('Sending email:' + to_email);

        }

      })

      response.context.action = "";

    }

    if (response.context.action === DISCOVERY_ACTION) {
      var actual_query = payload.input.text;
      try {
        if (response.context.nquery) {
          payload.input.text = response.context.nquery;
        }
        console.log('************** Discovery *************** InputText : ' + payload.input.text);
        endConversation = true;
        let discoveryResponse = '';
        
        if (!discoveryParams) {
          console.log('Discovery is not ready for query.');
          discoveryResponse = 'Sorry, currently I do not have a response. Discovery initialization is in progress. Please try again later.';
          if (response.output.text) {
            response.output.text.push(discoveryResponse);
          }
          // Clear the context's action since the lookup and append was attempted.
          response.context.action = {};
          // processResponse(null, response);
          // Clear the context's action since the lookup was attempted.
          payload.context.action = {};
        } else {
          const queryParams = {
            natural_language_query: payload.input.text,
            passages: true,
            filter: response.context.filter,
            count: 5
          };
          Object.assign(queryParams, discoveryParams);
          discovery.query(queryParams, (err, searchResponse) => {
            discoveryResponse = 'Sorry, currently I do not have a response. Our Customer representative will get in touch with you shortly.';
            //console.log('Output ', searchResponse.results[0]);
            console.log('************** Discovery *************** Response : ' + util.inspect(searchResponse, {showHidden: false, depth: null}));
            if (err) {
              console.error('Error searching for documents: ' + err);
            } else if ((response.context.discovery === 'discovery')) {

              if (searchResponse.passages.length > 0) {
                     const bestPassage = searchResponse.results[0];
              
                // Trim the passage to try to get just the answer part of it.
                const lines = bestPassage.html.split('\n');
				let firstLine = 'I found the following in my knowledgebase which might help:<br><br>';
                let bestLine = '';
                let questionFound = false;
                for (let i = 0, size = lines.length; i < size; i++) {
                  const line = lines[i].trim();
                  if (!line) {
                    continue; // skip empty/blank lines
                  }
                  if (line.includes('<h1') || line.includes('/h1>')) {
                    // To get the answer we needed to know the Q/A format of the doc.
                    // Skip questions which either have a '?' or are a header '<h1'...
                    questionFound = true;
                    continue;
                  }

                  bestLine += line; // Best so far, but can be tail of earlier answer.

                  if (questionFound && bestLine) {
                    // We found the first non-blank answer after the end of a question. Use it.
                    //break;
                  }
                }

                bestLine = htmlToText.fromString(bestLine, {
                  wordwrap: 80,
                  preserveNewlines: true,
                  singleNewLineParagraphs: true
                });
                bestLine = bestLine.replace(/\r?\n|\r/g, "<br>");
                bestLine += "<br><br>Press 'Yes' if this answers your query, 'No' if this did not answer your query or 'Show More' if you want to know more.<br><input type=\"submit\"  class=\"btn-primary\"  onclick=\"responseTypes('Yes')\"   value=\"Yes\" />  <input type=\"submit\"  class=\"btn-primary\"  onclick=\"responseTypes('No')\"   value=\"No\" />  <input type=\"submit\"  class=\"btn-primary\"  onclick=\"responseTypes('Show More')\"   value=\"Show More\" />";
                bestLine = firstLine+bestLine;

                discoveryResponse =
                  bestLine || 'Sorry I currently do not have an appropriate response for your query. Our customer care executive will call you in 24 hours.';
              }
            } else if (response.context.discovery === 'faq') {
             // console.log("Filter: " + response.context.filter);
              if (searchResponse.results.length > 0) {
                const bestPassage = searchResponse.results[0];
              //  console.log('Passage score: ', bestPassage.passage_score);
              //  console.log('Passage text: ', bestPassage.passage_text);

                // Trim the passage to try to get just the answer part of it.
                const lines = bestPassage.html.split('\n');
                let bestLine = '';
                let questionFound = false;
                for (let i = 0, size = lines.length; i < size; i++) {
                  const line = lines[i].trim();
                  if (!line) {
                    continue; // skip empty/blank lines
                  }
                  if (line.includes('<h1') || line.includes('/h1>')) {
                    // To get the answer we needed to know the Q/A format of the doc.
                    // Skip questions which either have a '?' or are a header '<h1'...
                    questionFound = true;
                    continue;
                  }

                  bestLine += line; // Best so far, but can be tail of earlier answer.

                  if (questionFound && bestLine) {
                    // We found the first non-blank answer after the end of a question. Use it.
                    //break;
                  }
                }

                bestLine = htmlToText.fromString(bestLine, {
                  wordwrap: 80,
                  preserveNewlines: true,
                  singleNewLineParagraphs: true
                });
                bestLine = bestLine.replace(/\r?\n|\r/g, "<br>");
                bestLine += "<br><br>Press 'Yes' if this answers your query, 'No' if this did not answer your query or 'Search' if you want to know more.<br><input type=\"submit\"  class=\"btn-primary\"  onclick=\"responseTypes('Yes')\"   value=\"Yes\" />  <input type=\"submit\"  class=\"btn-primary\"  onclick=\"responseTypes('No')\"   value=\"No\" />  <input type=\"submit\"  class=\"btn-primary\"  onclick=\"responseTypes('Search')\"   value=\"Search\" />";


                discoveryResponse =
                  bestLine || 'Sorry I currently do not have an appropriate response for your query. Our customer care executive will call you in 24 hours.';
                //  response.context.showoption = "Yes";
              }
            } else if (response.context.discovery === 'undefined') {
              if (searchResponse.results.length > 0) {
                //console.log ('within undefined');
                let bestLine
                let bestLines = 'I have found the following documents that might help:<br>';
                var encoded_url
                for (let i = 0, size = searchResponse.results.length; i < size; i++) {
                  //bestLine = searchResponse.results[i].enriched_text.relations[i].sentence.substring(0, 200);
                  // bestLine = searchResponse.results[i].text.substring(3, 200);
                  bestLine = searchResponse.results[i].text;
                  if (searchResponse.results[i].text.indexOf("no title") > -1) {
                    bestLine = searchResponse.results[i].text.substring(searchResponse.results[i].text.indexOf("no title") + 8);
                  }
                  bestLine = bestLine.substring(0, 200);
                  //console.log('text: '+bestLine);
                  // Check for prefix to get folder
                  var prefix = '';
                  if (searchResponse.results[i].extracted_metadata.filename.startsWith('EUC')) {
                    prefix = 'EUC';
                  } else if (searchResponse.results[i].extracted_metadata.filename.startsWith('OTC')) {
                    prefix = 'OTC';
                  } else if (searchResponse.results[i].extracted_metadata.filename.startsWith('P2Y')) {
                    prefix = 'P2Y';
                  } else if (searchResponse.results[i].extracted_metadata.filename.startsWith('MPS')) {
                    prefix = 'MPS';
                  }
                  // to get the filename				
                  var fext = searchResponse.results[i].extracted_metadata.filename.split('.').pop().toLowerCase();
                  var fc = searchResponse.results[i].extracted_metadata.filename.indexOf(fext, 1);
                  if (fext == 'docx') {
                    //fc = searchResponse.results[i].extracted_metadata.filename.indexOf("docx",1);
                    encoded_url = encodeURI(searchResponse.results[i].extracted_metadata.filename.substring(0, (fc + 4)))
                  } else if (fext == 'doc') {
                    encoded_url = encodeURI(searchResponse.results[i].extracted_metadata.filename.substring(0, (fc + 3)))
                  } else if (fext == 'pdf') {
                    encoded_url = encodeURI(searchResponse.results[i].extracted_metadata.filename.substring(0, (fc + 3)))
                  }
                  if (process.env.DOC_SERVER_ONLINE == 1) {
                    if (bestLine) {
                      bestLines += "<div class='base--table'>" + (i + 1) + ") " + bestLine + "...<br><br>For more details click <a href='http://oneplace.pmiapps.biz/collab/generic1/LERR/_layouts/15/WopiFrame.aspx?sourcedoc=" + process.env.HOST_URL + "_" + prefix + "/" + encoded_url + "&action=view' target=new>" + " here." + "</a></div><br>";

                    }
                  } else {

                    bestLines += "<div class='base--table'>" + (i + 1) + ") " + bestLine + "...<br><br>For more details click: PMI DOC SERVER OFFLINE</div><br>";
                  }
                }
                bestLines += "<br>Press 'Yes' if this answers your query, 'No' if this did not answer your query.<br><input type=\"submit\"  class=\"btn-primary\"  onclick=\"responseTypes('Yes')\"   value=\"Yes\" />  <input type=\"submit\"  class=\"btn-primary\"  onclick=\"responseTypes('No')\"   value=\"No\" />";
                discoveryResponse =
                  bestLines || 'Sorry I currently do not have an appropriate response for your query. Our customer care executive will call you in 24 hours.';
              }
            }
            //console.log("Txt: " + response.output.text);
            if (response.output.text) {
              response.output.text.push(discoveryResponse);
            }
            // Clear the context's action since the lookup and append was completed.
            response.context.action = {};
            // processResponse(null, response);
            // Clear the context's action since the lookup was completed.
            payload.context.action = {};
            endConversation = false;
            return res.json(updateMessage(payload, response));
          });
        }
      } catch (e) {
        console.log(e.message);
      }
    }


    if (!endConversation) {
      // console.log("Data1:", response.context);
      return res.json(updateMessage(payload, response));
    }

  }

  // Start conversation with empty message.
  //console.log("Data4:", response);
  conversation.message(payload, processResponse);


});

//************* CODE FOR OBJECT STORAGE *************

//fetch a single document from S3 storage
app.get("/files/:filename", function (request, response) {
    console.log('File Name: ' + request.params.filename)
    var adapter = skipperS3(s3config);
    var readStream = adapter.read(request.params.filename);
	readStream.pipe(response);
});


//************* CODE FOR OBJECT STORAGE *************


/**
 * Updates the response text using the intent confidence
 * @param  {Object} input The request to the Conversation service
 * @param  {Object} response The response from the Conversation service
 * @return {Object}          The response with the updated message
 */
function updateMessage(input, response) {

  senddataToCloudant(input, response);
  //response.output.text = autolinker.link((response.output.text).toString());
  response.output.text = convertToAutoLinkerFromArray(response.output.text);

  if (!response.output) {
    //  console.log("Data6:", response.output.text);
    response.output = {};
  } else {
    //console.log("Data3:", response.context);
    return response;
  }

}

function updateHindiMessage(response) {

  //senddataToCloudant(input, response);
  //response.output.text = autolinker.link((response.output.text).toString());
  //response.output.text = response;
  //convertToAutoLinkerFromArray(response);

  if (!response.output) {
    console.log("No Output!!!");
    response.output = {};
  } else {
    console.log("Data3:", response.output);
    return response;
  }

}

function senddataconversationToCloudantReport(conversationidmaster, fullnamemasterpara, callback) {
  if (conversationidmaster != "" && fullnamemasterpara != "") {
    var filename1 = "ConversationReport_" + conversationidmaster + ".xlsx";
    //	console.log (filename1);
    CloudantLoad.readreportconversationfromCloudant(conversationidmaster, filename1, (result) => {
      if (result) {
        console.log(result);
        callback(result);
      } else {
        filename1 = "";
        callback(result);
        console.log(result);
      }
    })
  }
}

function senddataToCloudantReport(req, response, callback) {
  var startdatelocal = "";
  var enddatelocal = "";
  if (req.body.input.startdatelocal != undefined) {
    startdatelocal = req.body.input.startdatelocal;
  }

  if (req.body.input.enddatelocal != undefined) {
    enddatelocal = req.body.input.enddatelocal;
  }

  var filename = "";
  if (startdatelocal != "" && enddatelocal != "") {
    filename = "ConversationReport" + Date.now() + ".xlsx";
    CloudantLoad.readreportfromCloudant(startdatelocal, enddatelocal, filename, response, (result) => {
      if (result) {
        callback(result);
      } else {
        filename = "";
        callback(result);
      }
    })
  }
}

function sendfeedbackToCloudant(rating, comment, conversationid, fullname, timestamp) {
  var messagefeedbacktocloudant = [

    {
      conversationid: conversationid,
      rating: rating,
      comment: comment,
      fullname: fullname,
      timestamp: timestamp
    }
  ]

  CloudantLoad.insertfeedbackToCloudant(messagefeedbacktocloudant);

}

function senddataToCloudantReportFeedback(req, response, callback) {
  var startdatelocal = "";
  var enddatelocal = "";
  if (req.body.input.startdatelocal != undefined) {
    startdatelocal = req.body.input.startdatelocal;
  }

  if (req.body.input.enddatelocal != undefined) {
    enddatelocal = req.body.input.enddatelocal;
  }

  var filename = "";
  if (startdatelocal != "" && enddatelocal != "") {
    filename = "FeedbackReport" + Date.now() + ".xlsx";
    CloudantLoad.readreportfromCloudantFeedback(startdatelocal, enddatelocal, filename, response, (result) => {
      if (result) {
        callback(result);
      } else {
        filename = "";
        callback(result);
      }
    })
  }
}

function senddataToCloudant(input, response) {
  var fullname = "";
  var email = "";
  var conversationid = "";
  var intent = "";
  var intentconfidence = "";
  var node = "";
  var dialog_turn_counter = "";
  var responsetext = "";

  if (response.context != undefined) {
    if (response.intents.length > 0) {
      intent = response.intents[0].intent;
      intentconfidence = response.intents[0].confidence;
    }
    if (response.output.nodes_visited > 0) {
      node = response.output.nodes_visited[0];
    }
    if (response.context.system.dialog_turn_counter != undefined) {
      dialog_turn_counter = response.context.system.dialog_turn_counter;
    }
    if (response.context.conversation_id != undefined) {
      // console.log("Hello   -----" + response.context.conversation_id);
      conversationid = response.context.conversation_id;
      conversationidmaster = conversationid;
    }

    if (response.context.name != undefined) {
      //  console.log("Hello   -----" + response.context.name);
      fullname = response.context.name;
      fullnamemaster = fullname;
    } else {
      // console.log("response.context.fname not found");

    }

    if (response.context.email != undefined) {
     // console.log("Hello   -----" + response.context.email);
      email = response.context.email;

    } else {
      // console.log("response.context.email not found");

    }

  } else {
    console.log("response.context not found");
  }
  if (response.output != undefined) {
    responsetext = htmlToText.fromString(response.output.text, {
      wordwrap: 80,
      preserveNewlines: true,
      singleNewLineParagraphs: true
    });

  }


  var messagetocloudant = [

    {
      conversationid: conversationid,
      input: response.input.text,
      response: responsetext,
      intent: intent,
      intentconfidence: intentconfidence,
      nodevisited: node,
      dialogturncounter: dialog_turn_counter,
      fullname: fullname,
      email: email,
      timestamp: dateTime()
    }
  ]

  CloudantLoad.insertMessageToCloudant(messagetocloudant);

}

function convertToAutoLinkerFromArray(responsetext) {
  var outputtext = "";
  if (responsetext != undefined && responsetext.length > 0) {
    for (var i = 0; i < responsetext.length; i++) {
      outputtext = outputtext + autolinker.link((responsetext[i]).toString());
    }

  }
  return outputtext;
}


function checkfileExists(fs, filename) {
  try {
    fs.statSync(filename);
  } catch (err) {
    if (err.code == 'ENOENT') return false;
  }
  return true;
}

function search(array, key, prop) {
  // Optional, but fallback to key['name'] if not selected
  prop = (typeof prop === 'undefined') ? 'name' : prop;

  for (var i = 0; i < array.length; i++) {
    if (array[i][prop] === key) {
      return array[i];
    }
  }
}

module.exports = app;