
/* global ConversationPanel: true, PayloadPanel: true*/
/* eslint no-unused-vars: "off" */
function calenderLoad(){
  $(function(){
    $('[type="date"].max-today').prop('max', function(){
        return new Date().toJSON().split('T')[0];
    });
  });
}


// Other JS files required to be loaded first: apis.js, conversation.js, payload.js
(function() {
  // Initialize all modules
  document.getElementById('FeedbackModal').style.display ="none";
      
      document.getElementById('Feedback').style.display ="none";
  var lang = document.getElementById('langId').value;
  //document.getElementById('yourFeedback').style.display ="none";
  // document.getElementById('FeedbackResult').style.display ="none";
  // document.getElementById('FeedbackImage').style.display ="none";
  // document.getElementById('FeedbackImageFaded').style.display ="block";
  // document.getElementById('star-1').checked = false;
  // document.getElementById('star-2').checked = false;
  // document.getElementById('star-3').checked = false;
  // document.getElementById('star-4').checked = false;
  // document.getElementById('star-5').checked = false;
  
  
  if(lang == "hindi"){
    ConversationPanel.hindiInit();
  }
  else{
    ConversationPanel.init();
  }
  
  
  PayloadPanel.init();
  
})();


