'use strict';

const fs = require('fs'); // file system for loading JSON
var Excel = require('exceljs');
const Cloudant = require('cloudant');

var cloudant = Cloudant({
    url: process.env.CLOUDANT_URL
  }),
  dbname = process.env.CLOUDANT_DB,
  db = cloudant.db.use(dbname);
var CloudantLoad = (function () {
  var id = Math.round((Math.random() * 1000000));


  function createcsv(result, reportfilename, responsesent, callback) {
    if (result.docs != undefined && result.docs.length > 0) {
      var workbook = new Excel.Workbook();
      var sheet = workbook.addWorksheet('Report');
      var worksheet = workbook.getWorksheet('Report');
      var path = './report/';
      worksheet.columns = [{
          header: 'Conversationid',
          key: 'conversationid',
          width: 30
        },
        {
          header: 'Input',
          key: 'input',
          width: 32
        },
        {
          header: 'Response',
          key: 'response',
          width: 64
        },
        {
          header: 'Dialogturncounter',
          key: 'dialogturncounter',
          width: 24
        },
        {
          header: 'Fullname',
          key: 'fullname',
          width: 20
        },
        {
          header: 'Timestamp',
          key: 'timestamp',
          width: 20
        }
      ];
      for (var i = 0; i < result.docs.length; i++) {
        worksheet.addRow({
          conversationid: result.docs[i].conversationid,
          input: result.docs[i].input,
          response: result.docs[i].response,
          dialogturncounter: result.docs[i].dialogturncounter,
          fullname: result.docs[i].fullname,
          timestamp: result.docs[i].timestamp
        }).commit();
      }
      workbook.commit;
      //workbook.csv.writeFile(path+reportfilename);
      workbook.xlsx.writeFile(path + reportfilename)
        .then(function () {
          callback(true);
        });
    }

  }

  function createcsvFeedback(result, reportfilename, responsesent, callback) {
    if (result.docs != undefined && result.docs.length > 0) {
      var workbook = new Excel.Workbook();
      var sheet = workbook.addWorksheet('Report');
      var worksheet = workbook.getWorksheet('Report');
      var path = './report/';
      worksheet.columns = [{
          header: 'Conversationid',
          key: 'conversationid',
          width: 30
        },
        {
          header: 'Rating',
          key: 'rating',
          width: 4
        },
        {
          header: 'Comment',
          key: 'comment',
          width: 64
        },
        {
          header: 'Fullname',
          key: 'fullname',
          width: 20
        },
        {
          header: 'Timestamp',
          key: 'timestamp',
          width: 20
        }
      ];
      for (var i = 0; i < result.docs.length; i++) {
        if (result.docs[i].rating != undefined) {
          worksheet.addRow({
            conversationid: result.docs[i].conversationid,
            rating: result.docs[i].rating,
            comment: result.docs[i].comment,
            fullname: result.docs[i].fullname,
            timestamp: result.docs[i].timestamp
          }).commit();
        }
      }
      workbook.commit;
      //workbook.csv.writeFile(path+reportfilename);
      workbook.xlsx.writeFile(path + reportfilename)
        .then(function () {
          callback(true);
        });
    }

  }

  function init() {}

  function insertMessageToCloudant(messageToCloudant) {

    db.bulk({
      docs: messageToCloudant
    }, function (er) {
      if (er) {
        throw er;
      }
    });

  }

  function readreportfromCloudant(startdate, enddate, reportfilename, responsesent, callback) {
    var timestamp = {
      name: 'timestamp',
      type: 'json',
      index: {
        fields: ['timestamp']
      }
    }
    db.index(timestamp, function (er, response) {
      if (er) {
        throw er;
      }

    });

    db.find({
      selector: {
        "$and": [{
          timestamp: {
            "$gte": startdate
          }
        }, {
          timestamp: {
            "$lte": enddate
          }
        }]
      },
      "fields": [
        "conversationid",
        "input",
        "response",
        "dialogturncounter",
        "fullname",
        "timestamp"
      ],
      "sort": [{
        "timestamp": "asc"
      }]
    }, function (er, result) {
      if (er) {
        console.log(er);
        throw er;
      } else {
        if (result.docs != undefined && result.docs.length > 0) {
          createcsv(result, reportfilename, responsesent);
          //sendemail(reportfilename);
          callback(reportfilename);
        } else {
          callback("No data found");
        }
      }




    });
    //console.log("readreport ended");
    //return a ;
  }

  function readreportfromCloudantFeedback(startdate, enddate, reportfilename, responsesent, callback) {
    var timestamp = {
      name: 'timestamp',
      type: 'json',
      index: {
        fields: ['timestamp']
      }
    }
    db.index(timestamp, function (er, response) {
      if (er) {
        throw er;
      }

    });
    db.find({
        selector: {
          "$and": [{
            timestamp: {
              "$gte": startdate
            }
          }, {
            timestamp: {
              "$lte": enddate
            }
          }],
          "rating": {
            "$exists": true
          }
        },
        "fields": [
          "conversationid",
          "rating",
          "comment",
          "fullname",
          "timestamp"
        ],
        "sort": [{
          "timestamp": "asc"
        }]
      }, function (er, result) {
        if (er) {
          console.log(er);
          throw er;
        } else {
          if (result.docs != undefined && result.docs.length > 0) {
            createcsvFeedback(result, reportfilename, responsesent, (result) => {
              if (result) {

                callback(reportfilename);
              } else {
                callback("No data found");
              }
            })
          }
		  else
          {
             callback("No data found");
          } 
        }
      }

    );

  }

  function readreportconversationfromCloudant(conversationid, reportfilename, callback) {
    var conversid = {
      name: 'conversationid',
      type: 'json',
      index: {
        fields: ['conversationid']
      }
    }
    db.index(conversid, function (er, response) {
      if (er) {
        throw er;
      }
     console.log('Index creation result: %s', response.result);
    });

    db.find({
        selector: {
          conversationid: conversationid
        },
        "fields": [
          "conversationid",
          "input",
          "response",
          "dialogturncounter",
          "fullname",
          "timestamp"
        ]
      }, function (er, result) {
        if (er) {
          console.log(er);
          throw er;
        } else {
          //console.log ("cid:"+conversationid);
          //console.log ("results:"+result);
          if (result.docs != undefined && result.docs.length > 0) {
            var responsesent = "";
			result.docs = result.docs.sortBy('dialogturncounter');
            console.log('I have called the create function');
            createcsv(result, reportfilename, responsesent, (result) => {
              if (result) {

                callback(reportfilename);
              } else {
                callback("No data found");
              }
            })
          }
		  else
          {
             callback("No data found");
          } 
        }
      }

    );

  }
  
  Array.prototype.sortBy = function(p) {
  return this.slice(0).sort(function(a,b) {
    return (a[p] > b[p]) ? 1 : (a[p] < b[p]) ? -1 : 0;
  });
}

  function insertfeedbackToCloudant(messagefeedbacktocloudant) {

    db.bulk({
      docs: messagefeedbacktocloudant
    }, function (er) {
      if (er) {
        throw er;
      }
    });

  }
  return {
    init: init,
    insertMessageToCloudant: insertMessageToCloudant,
    insertfeedbackToCloudant: insertfeedbackToCloudant,
    readreportconversationfromCloudant: readreportconversationfromCloudant,
    readreportfromCloudant: readreportfromCloudant,
    readreportfromCloudantFeedback: readreportfromCloudantFeedback
  };
}());
module.exports = CloudantLoad;