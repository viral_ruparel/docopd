 var startdatelocal = "";
 var enddatelocal = "";
 var reportfilename = "";


 function generateReport() {
   var startdateentered = document.getElementById("startdate").value;
   var enddateentered = document.getElementById("enddate").value;


   if (!validateDate(startdateentered)) {

     document.getElementById('datevalidation').style.display = "block";
     document.getElementById('datevalidation').innerHTML = "Provide proper start date in MM/DD/YY format";
     return;
   }
   if (!validateDate(enddateentered)) {
     document.getElementById('datevalidation').style.display = "block";
     document.getElementById('datevalidation').innerHTML = "Provide proper end date in MM/DD/YY format";
     return;
   }

   if (enddateentered.trim() == "") {
     document.getElementById('datevalidation').style.display = "block";
     document.getElementById('datevalidation').innerHTML = "Provide proper end date in MM/DD/YY format";
     return;

   }
   if (startdateentered.trim() == "") {
     document.getElementById('datevalidation').style.display = "block";
     document.getElementById('datevalidation').innerHTML = "Provide proper start date  in MM/DD/YY format";
     return;

   }
   if (startdateentered.trim() != "" && enddateentered.trim() != "") {
     document.getElementById('datevalidation').style.display = "none";
     document.getElementById('reportFormResultInProgress').style.display = "block";
     document.getElementById('reportForm').style.display = "none";
     startdatelocal = "";
     enddatelocal = "";
     startdatelocal = convert(startdateentered) + " 00:00:01";
     enddatelocal = convert(enddateentered) + " 23:59:59";
     var messageEndpoint = '/api/report';
     var http = new XMLHttpRequest();
     var parametertoCloudant = {};
     if (startdatelocal) {
       parametertoCloudant.input = {
         startdatelocal: startdatelocal,
         enddatelocal: enddatelocal
       };
     }
     reportfilename = "";
     var params = JSON.stringify(parametertoCloudant);
     http.open('POST', messageEndpoint, true);
     http.setRequestHeader('Content-type', 'application/json');
     http.onreadystatechange = function () {

       //alert(http.responseText);      
       if (http.readyState === 4 && http.status === 200 && http.responseText) {
         var a = http.responseText.replace(/['"]+/g, '');
         if (a != "No data found") {

           document.getElementById('reportFormResult').style.display = "block";
           document.getElementById('reportFormResultInProgress').style.display = "none";
           //document.getElementById('reportFormResultlink').href = a;
           var url = '/api/download';
           var params = 'filename=' + a;
           document.getElementById('reportFormResultlink').href = url + "?" + params;
           reportfilename = a;
         } else {
           document.getElementById('reportForm').style.display = "none";
           document.getElementById('reportFormResult').style.display = "none";
           document.getElementById('reportFormResultInProgress').style.display = "none";
           document.getElementById('reportFormResultNodataFound').style.display = "block";
           document.getElementById('reportFormResultlink').href = "";
           reportfilename = "";
           a = "";

         }


       }
     };
     http.send(params);
   } else {
     document.getElementById('datevalidation').style.display = "block";

   }


 }

 function generateFeedbackReport() {
   var startdateentered = document.getElementById("startdate").value;
   var enddateentered = document.getElementById("enddate").value;


   if (!validateDate(startdateentered)) {

     document.getElementById('datevalidation').style.display = "block";
     document.getElementById('datevalidation').innerHTML = "Provide proper start date in MM/DD/YY format";
     return;
   }
   if (!validateDate(enddateentered)) {
     document.getElementById('datevalidation').style.display = "block";
     document.getElementById('datevalidation').innerHTML = "Provide proper end date in MM/DD/YY format";
     return;
   }

   if (enddateentered.trim() == "") {
     document.getElementById('datevalidation').style.display = "block";
     document.getElementById('datevalidation').innerHTML = "Provide proper end date in MM/DD/YY format";
     return;

   }
   if (startdateentered.trim() == "") {
     document.getElementById('datevalidation').style.display = "block";
     document.getElementById('datevalidation').innerHTML = "Provide proper start date  in MM/DD/YY format";
     return;

   }
   if (startdateentered.trim() != "" && enddateentered.trim() != "") {
     document.getElementById('datevalidation').style.display = "none";
     document.getElementById('reportFormResultInProgress').style.display = "block";
     document.getElementById('reportForm').style.display = "none";
     startdatelocal = "";
     enddatelocal = "";
     startdatelocal = convert(startdateentered) + " 00:00:01";
     enddatelocal = convert(enddateentered) + " 23:59:59";
     var messageEndpoint = '/api/feedbackreport';
     var http = new XMLHttpRequest();
     var parametertoCloudant = {};
     if (startdatelocal) {
       parametertoCloudant.input = {
         startdatelocal: startdatelocal,
         enddatelocal: enddatelocal
       };
     }
     reportfilename = "";
     var params = JSON.stringify(parametertoCloudant);
     http.open('POST', messageEndpoint, true);
     http.setRequestHeader('Content-type', 'application/json');
     http.onreadystatechange = function () {
       //alert(http.responseText);      
       if (http.readyState === 4 && http.status === 200 && http.responseText) {
         var a = http.responseText.replace(/['"]+/g, '');
         if (a != "No data found") {

           document.getElementById('reportFormResult').style.display = "block";
           document.getElementById('reportFormResultInProgress').style.display = "none";
           //document.getElementById('reportFormResultlink').href = a;
           var url = '/api/download';
           var params = 'filename=' + a;
           document.getElementById('reportFormResultlink').href = url + "?" + params;
           reportfilename = a;
         } else {
           document.getElementById('reportForm').style.display = "none";
           document.getElementById('reportFormResult').style.display = "none";
           document.getElementById('reportFormResultInProgress').style.display = "none";
           document.getElementById('reportFormResultNodataFound').style.display = "block";
           document.getElementById('reportFormResultlink').href = "";
           reportfilename = "";
           a = "";

         }


       }
     };
     http.send(params);
   } else {
     document.getElementById('datevalidation').style.display = "block";

   }


 }

 function OpenReportInput() {
   document.getElementById('reportForm').style.display = "block";
   document.getElementById('reportFormResult').style.display = "none";
   document.getElementById('reportFormResultNodataFound').style.display = "none";
   document.getElementById('datevalidation').style.display = "none";
   document.getElementById('reportFormResultlink').href = "";
   reportfilename = "";

 }

 function convert(usDate) {
   var dateParts = usDate.split(/(\d{1,2})[\/ -](\d{1,2})[\/ -](\d{4})/);
   return dateParts[3] + "-" + dateParts[1] + "-" + dateParts[2];
 }

 function validateDate(testdate) {
   var date_regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
   return date_regex.test(testdate);
 }