// The ConversationPanel module is designed to handle
// all display and behaviors of the conversation column of the app.
/* eslint no-unused-vars: "off" */
/* global Api: true, Common: true*/
/* global document, window*/
/* exported ConversationPanel*/
/* eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */

var initial = true;
var fullnameavailable = false;

//const Cloudant = require('cloudant');
var ConversationPanel = (function () {
  var settings = {
    selectors: {
      chatBox: '#scrollingChat',
      fromUser: '.from-user',
      fromWatson: '.from-watson',
      latest: '.latest',
      textInputLocation: '#textInputLocation',
      loginSection: '#loginSection',
      conversationSection: '#conversationSection',
      textInputOne: '#textInputOne'
    },
    authorTypes: {
      user: 'user',
      watson: 'watson'
    }
  };
  var countDownTimer;
  var feedbackcomment = "";
  var currentValue = 0;
  // Publicly accessible methods defined
  return {
    init: init,
    hindiInit: hindiInit,
    inputKeyDown: inputKeyDown,
    hindiInputKeyDown: hindiInputKeyDown,
    inputKeyDownFB: inputKeyDownFB,
    btnLoginClicked: btnLoginClicked,
    btnFBSend: btnFBSend,
    btnFBOpen: btnFBOpen,
    btnFeedback: btnFeedback,
    btnFBClose: btnFBClose,
    menuClicked: menuClicked,
    // TimeOutTrigger method defined next
    timeOutTrigger: timeOutTrigger,
    btnCDClose: btnCDClose,
    countDownCall: countDownCall
  };

  // Initialize the module
  function init() {
    chatUpdateSetup();
    Api.sendRequest('Hello', null);
    initial = false;
    setupInputBox();
  }

  function hindiInit() {
    chatUpdateSetup();
    Api.sendHindiRequest('नमस्कार', null);
    initial = false;
    setupInputBox();
  }
  // Set up callbacks on payload setters in Api module
  // This causes the displayMessage function to be called when messages are sent / received
  function chatUpdateSetup() {
    var currentRequestPayloadSetter = Api.setRequestPayload;
    Api.setRequestPayload = function (newPayloadStr) {
      currentRequestPayloadSetter.call(Api, newPayloadStr);
      displayMessage(JSON.parse(newPayloadStr), settings.authorTypes.user);
    };

    var currentResponsePayloadSetter = Api.setResponsePayload;
    Api.setResponsePayload = function (newPayloadStr) {
      console.log("*****Hindi Response in setresponsepayload**************", newPayloadStr);
      currentResponsePayloadSetter.call(Api, newPayloadStr);
      displayMessage(JSON.parse(newPayloadStr), settings.authorTypes.watson);
      if (JSON.parse(newPayloadStr).context.name != "") {
        if (!fullnameavailable) {

          //document.getElementById('FeedbackImageFaded').style.display ="none";
          //document.getElementById('FeedbackImage').style.display ="block";
          fullnameavailable = true;

        }

      }
    };
  }

  function setupInputBox() {
    var input = document.getElementById('textInput');
    var dummy = document.getElementById('textInputDummy');
    var padding = 3;

    if (dummy === null) {
      var dummyJson = {
        tagName: "div",
        attributes: [{
          name: "id",
          value: "textInputDummy"
        }]
      };

      dummy = Common.buildDomElement(dummyJson);
      ['font-size', 'font-style', 'font-weight', 'font-family', 'line-height', 'text-transform', 'letter-spacing'].forEach(function (index) {
        if (input !== null) {
          dummy.style[index] = window.getComputedStyle(input, null).getPropertyValue(index);
        }
      });

      document.body.appendChild(dummy);
    }

    if (input !== null) {
      input.addEventListener('input', function () {
        if (this.value === '') {
          this.classList.remove('underline');
          this.setAttribute('style', 'width:' + '100%');
          this.style.width = '100%';
        } else {
          this.classList.add('underline');
          var txtNode = document.createTextNode(this.value);
          dummy.textContent = txtNode.textContent;
          var widthValue = (dummy.offsetWidth + padding) + 'px';
          this.setAttribute('style', 'width:' + widthValue);
          this.style.width = widthValue;
        }
      });
      Common.fireEvent(input, 'input');
    }
  }

  // Display a user or Watson message that has just been sent/received
  function displayMessage(newPayload, typeValue) {
    if (initial)
      return;
    var isUser = isUserMessage(typeValue);
    var textExists = (newPayload.input && newPayload.input.text)
      || (newPayload.output && newPayload.output.text);
    if (isUser !== null && textExists) {
      // Create new message DOM element
      var messageDivs = buildMessageDomElements(newPayload, isUser);
      var chatBoxElement = document.querySelector(settings.selectors.chatBox);
      var previousLatest = chatBoxElement.querySelectorAll((isUser
        ? settings.selectors.fromUser : settings.selectors.fromWatson)
        + settings.selectors.latest);
      // Previous "latest" message is no longer the most recent
      if (previousLatest) {
        Common.listForEach(previousLatest, function (element) {
          element.classList.remove('latest');
        });
      }

      messageDivs.forEach(function (currentDiv) {
        chatBoxElement.appendChild(currentDiv);
        // Class to start fade in animation
        currentDiv.classList.add('load');
      });
      // Move chat to the most recent messages when new messages are added
      scrollToChatBottom();
    }
  }

  // Checks if the given typeValue matches with the user "name", the Watson "name", or neither
  // Returns true if user, false if Watson, and null if neither
  // Used to keep track of whether a message was from the user or Watson
  function isUserMessage(typeValue) {
    if (typeValue === settings.authorTypes.user) {
      return true;
    } else if (typeValue === settings.authorTypes.watson) {
      return false;
    }
    return null;
  }

  // Constructs new DOM element from a message payload
  function buildMessageDomElements(newPayload, isUser) {
    var currentText = isUser ? newPayload.input.text : newPayload.output.text;
    if (Array.isArray(currentText)) {
      //currentText = currentText.join('<br/>');
      //FIX for empty string at UI
      currentText = currentText.filter(function (val) { return val; }).join('<br/>');

    }
    var messageArray = [];
    var summary = '&nbsp';
    var type = typeof (newPayload.context);
    console.log("Type: ", type);
    if (type != "undefined" && !isUser) {
      summary = newPayload.context.summary;
    }

    /**
     * Bhaskar Konar: Image tag implementation
     */
    if (currentText) {
      var messageJson = {
        // <div class='segments'>
        'tagName': 'div',
        'classNames': ['segments'],
        'children': [{
          // <div class='from-user/from-watson latest'>
          'tagName': 'div',
          'classNames': [(isUser ? 'from-user' : 'from-watson'), 'latest', ((messageArray.length === 0) ? 'top' : 'sub')],
          'children': [{
            'tagName': 'div',
            'classNames': ['summary'],
            'text': summary
          }, {
            // <div class='message-inner'>
            'tagName': 'div',
            'classNames': ['message-inner'],
            'children': []
          }]
        }]
      };



      var listOfMsgs = transformTextIntoList(currentText);
      var messageJson = generateDomMessage(messageJson, listOfMsgs);

      /**
       * Bhaskar Konar: End Image tag implementation
       */

      messageArray.push(Common.buildDomElement(messageJson));
    }

    return messageArray;
  }

  // Scroll to the bottom of the chat window (to the most recent messages)
  // Note: this method will bring the most recent user message into view,
  //   even if the most recent message is from Watson.
  //   This is done so that the "context" of the conversation is maintained in the view,
  //   even if the Watson message is long.
  function scrollToChatBottom() {
    var scrollingChat = document.querySelector('#scrollingChat');

    // Scroll to the latest message sent by the user
    var scrollEl = scrollingChat.querySelector(settings.selectors.fromUser + settings.selectors.latest);
    if (scrollEl) {
      scrollingChat.scrollTop = scrollEl.offsetTop;
    }
  }

  // Handles the submission of input
  function inputKeyDown(event, inputBox) {
    // Submit on enter key, dis-allowing blank messages
    if (inputBox.value && (event.keyCode === 13 || event.type === 'click')) {
      // Retrieve the context from the previous server response
      var context;
      var latestResponse = Api.getResponsePayload();
      if (latestResponse) {
        context = latestResponse.context;
      }

      // Send the user message
      Api.sendRequest(inputBox.value, context);

      // Clear input box for further messages
      inputBox.value = '';
      Common.fireEvent(inputBox, 'input');
    }
  }

  // Handles the submission of input
  function hindiInputKeyDown(event, inputBox) {
    // Submit on enter key, dis-allowing blank messages
    if (inputBox.value && (event.keyCode === 13 || event.type === 'click')) {
      // Retrieve the context from the previous server response
      var context;
      var latestResponse = Api.getResponsePayload();
      if (latestResponse) {
        context = latestResponse.context;
      }

      // Send the user message
      Api.sendHindiRequest(inputBox.value, context);

      // Clear input box for further messages
      inputBox.value = '';
      Common.fireEvent(inputBox, 'input');
    }
  }

  function inputKeyDownFB(event, inputBox4) {
    feedbackcomment = inputBox4.value;

  }

  //Handles the click event of login
  // Not used any longer
  function btnLoginClicked(event) {
    if (event.keyCode === undefined || event.keyCode === 13) {
      /*if(settings.selectors.textInputLocation.value === '') {
        return;
      }*/

      settings.selectors.loginSection.classList.add('hide');
      settings.selectors.conversationSection.classList.remove('hide');
      settings.selectors.textInputOne.focus();
    }
  }
  function btnFBSend(event) {
    //Single Star Comment Validation Start
    console.log("Stars : " + currentValue + "feedbackcomment: '" + feedbackcomment.trim().length + "'");
    if (currentValue <= 2 && feedbackcomment.trim().length == 0) {
      document.getElementById('FeedBackTextArea').value = ''; //Clearing TextArea
      document.getElementById('ErrorMessage').style.display = "block";
      return;
    }
    //Single Star Comment Validation End
    Api.sendFeedback(currentValue, feedbackcomment);
    document.getElementById('FeedbackModal').style.display = "none";
    document.getElementById('Feedback').style.display = "none";
    document.getElementById('yourFeedback').style.display = "none";
    document.getElementById('star-1').checked = false;
    document.getElementById('star-2').checked = false;
    document.getElementById('star-3').checked = false;
    document.getElementById('star-4').checked = false;
    document.getElementById('star-5').checked = false;
    currentValue = 0;


    document.getElementById('ErrorMessage').style.display = "none"; //ErrorMessage hidden	
    document.getElementById('contentParent').style.display = "none";
    document.getElementById('TimeoutResult').style.display = "none";  //TimeoutResult hidden
    document.getElementById('FeedbackResult').style.display = "block";
  }
  function btnFBOpen(event) {
    document.getElementById('FeedbackModal').style.display = "block";
    document.getElementById('Feedback').style.display = "block";
    document.getElementById('bodymaster').style.display = "block";
    //document.getElementById('FeedbackImage').style.display ="none";
    // document.getElementById('ErrorMessage').style.display ="none"; //ErrorMessage hidden	


  }
  function btnFeedback(myRadio) {
    currentValue = myRadio.value;
    document.getElementById('yourFeedback').style.display = "block";
  }
  function btnFBClose(event) {
    document.getElementById('FeedbackModal').style.display = "none";
    document.getElementById('Feedback').style.display = "none";
    // document.getElementById('yourFeedback').style.display ="none";
    //document.getElementById('FeedbackImage').style.display ="block";
    // document.getElementById('star-1').checked = false;
    // document.getElementById('star-2').checked = false;
    // document.getElementById('star-3').checked = false;
    // document.getElementById('star-4').checked = false;
    // document.getElementById('star-5').checked = false;
    // currentValue = 0;
  }
  //Close button popup function of session timeout counter code start
  function btnCDClose(event) {
    console.log("Close pressed");
    clearInterval(countDownTimer);
    document.getElementById("Timer").innerHTML = "";
    document.getElementById('CountDownModal').style.display = "none";
    document.getElementById('CountDown').style.display = "none";
    document.getElementById('FeedbackModal').style.display = "none";
    document.getElementById('Feedback').style.display = "none";
    // document.getElementById('yourFeedback').style.display ="none";
    // document.getElementById('FeedbackImage').style.display ="block";
    // document.getElementById('star-1').checked = false;
    // document.getElementById('star-2').checked = false;
    // document.getElementById('star-3').checked = false;
    // document.getElementById('star-4').checked = false;
    // document.getElementById('star-5').checked = false;
    // currentValue = 0;
  }
  //Close button popup function of session timeout counter code end

  //Handles the click event of Menu button
  //Not being used anymore
  function menuClicked(event) {
    var drawer = document.getElementsByClassName('drawer')[0],
      closeDrawer = document.getElementsByClassName('close-drawer')[0];

    event.preventDefault();
    if (drawer.classList.contains('open')) {
      drawer.classList.remove('open');
    } else {
      drawer.classList.add('open');
      drawer.classList.remove('close');
    }

    closeDrawer.addEventListener('click', function (e) {
      e.preventDefault();
      drawer.classList.remove('open');
    });

  }
  //Countdown for seesion end code start
  function countDownCall(min, sec) {
    console.log("countDownCall");
    console.log(min);
    console.log(sec);
    document.getElementById('bodymaster').style.display = "block";
    document.getElementById('CountDownModal').style.display = "block";
    document.getElementById('CountDown').style.display = "block";
    document.getElementById('FeedbackModal').style.display = "none";
    document.getElementById('Feedback').style.display = "none";
    document.getElementById('yourFeedback').style.display = "none";
    document.getElementById('star-1').checked = false;
    document.getElementById('star-2').checked = false;
    document.getElementById('star-3').checked = false;
    document.getElementById('star-4').checked = false;
    document.getElementById('star-5').checked = false;
    currentValue = 0;
    document.getElementById('FeedbackResult').style.display = "none";
    document.getElementById('TimeoutResult').style.display = "none";
    var minutes = min;
    var seconds = sec;
    // Update the count down every 1 second
    countDownTimer = setInterval(function () {

      // Output the result in an element with id="demo"
      document.getElementById("Timer").innerHTML = minutes + "m " + seconds + "s ";
      seconds -= 1
      if (seconds < 0) {
        minutes -= 1
        seconds = 59
      }
      // If the count down is over, write some text 
      if (minutes < 0) {
        clearInterval(countDownTimer);
        ConversationPanel.timeOutTrigger(this);
      }
    }, 1000);

  }
  //Countdown for seesion end code end
  //TimeOut Function Start 
  function timeOutTrigger(event) {
    document.getElementById('FeedbackModal').style.display = "none";
    document.getElementById('Feedback').style.display = "none";
    document.getElementById('yourFeedback').style.display = "none";
    document.getElementById('star-1').checked = false;
    document.getElementById('star-2').checked = false;
    document.getElementById('star-3').checked = false;
    document.getElementById('star-4').checked = false;
    document.getElementById('star-5').checked = false;
    currentValue = 0;
    document.getElementById('contentParent').style.display = "none";
    document.getElementById('FeedbackResult').style.display = "none";
    document.getElementById('CountDownModal').style.display = "none";
    document.getElementById('TimeoutResult').style.display = "block";

  }
  //TimeOut Function End

  

  function transformTextIntoList(text) {
    var imgVideoRegex = /(<img>.*?<\/img>|<video>.*?<\/video>)/g;
    var resultList = [];
    if (text.match(imgVideoRegex)) {
      var resultList = text.split(imgVideoRegex);
    } else {
      resultList.push(text);
    }
    return resultList;
  }
 

  function generateDomMessage(messageJson, messageList) {
    var generatedList;
    var imgRegex = /<img>(.*?)<\/img>/g;
    var videoRegex = /<video>\s*(.*?)\s*<\/video>/g;

    for (var i = 0; i < messageList.length; i++) {
      if (messageList[i].match(imgRegex)) {
        var img = {
          // <p>{messageText}</p>
          'tagName': 'img',
          attributes: [{ 'name': 'src', 'value': '/files/' + RegExp.$1 }]
        };
        messageJson.children[0].children[1].children.push(img);

      } else if (messageList[i].match(videoRegex)) {
        var video = {
          // <p>{messageText}</p>
          'tagName': 'video',
          'attributes': [{ 'name': 'controls' }],
          'children': [{
            'tagName': 'source',
            'attributes': [{ 'name': 'src', 'value': 'https://wvsupportadvisorsboxdev.mybluemix.net/files/' + RegExp.$1 }],
            'text': 'This browser does not support video'
          }]

        };
        messageJson.children[0].children[1].children.push(video);

      } else {
        var txt = {
          // <p>{messageText}</p>
          'tagName': 'p',
          'text': messageList[i]
        };
        messageJson.children[0].children[1].children.push(txt);
      }
    }
    return messageJson;
  }
  
}());
